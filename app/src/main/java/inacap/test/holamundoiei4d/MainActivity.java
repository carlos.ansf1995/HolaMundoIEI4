package inacap.test.holamundoiei4d;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import inacap.test.holamundoiei4d.controlador.UsuariosController;
import inacap.test.holamundoiei4d.modelo.sqlite.HolaMundoDBContract;
import inacap.test.holamundoiei4d.vista.FormularioActivity;
import inacap.test.holamundoiei4d.vista.HomeActivity;
import inacap.test.holamundoiei4d.vista.HomeDrawerActivity;

public class MainActivity extends AppCompatActivity {

    private EditText editTextUsername, editPassword;
    private Button btLogin;
    private TextView tvUsername, tvRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Revisamos si la persona ya ha iniciado sesion
        SharedPreferences sesiones = getSharedPreferences(HolaMundoDBContract.HolaMundoSesion.SHARED_PREFERECENCES_NAME, Context.MODE_PRIVATE);
        boolean yaInicioSesion = sesiones.getBoolean(HolaMundoDBContract.HolaMundoSesion.FIELD_SESION, false);
        if(yaInicioSesion){
            Intent i = new Intent(MainActivity.this, HomeDrawerActivity.class);
            startActivity(i);
            finish();
        }

        this.editTextUsername = (EditText) findViewById(R.id.etUsername);
        this.editPassword = (EditText) findViewById(R.id.etPassword);

        this.btLogin = (Button) findViewById(R.id.btLogin);
        this.tvUsername = (TextView) findViewById(R.id.tvUsername);
        this.tvRegistrar = (TextView) findViewById(R.id.tvRegistrar);

        this.btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Validar datos
                String username = editTextUsername.getText().toString();
                String password = editPassword.getText().toString();

                UsuariosController controller = new UsuariosController(getApplicationContext());
                try {
                    boolean login = controller.usuarioLogin(username, password);

                    if(login){
                        Intent i = new Intent(MainActivity.this, HomeDrawerActivity.class);
                        startActivity(i);
                        // La persona inicio sesion, debemos guardar
                        // sus datos de sesion

                        SharedPreferences sesiones = getSharedPreferences(HolaMundoDBContract.HolaMundoSesion.SHARED_PREFERECENCES_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sesiones.edit();

                        // Guardamos un boolean que diga si la persona inicio sesion
                        editor.putBoolean(HolaMundoDBContract.HolaMundoSesion.FIELD_SESION, true);
                        editor.putString(HolaMundoDBContract.HolaMundoSesion.FIELD_USERNAME, username);

                        editor.commit();


                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "Datos incorrectos", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e){
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        this.tvRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Iniciar la segunda ventana
                Intent nuevaVentana = new Intent(MainActivity.this, FormularioActivity.class);
                startActivity(nuevaVentana);
            }
        });

    }
}











